# Telecaster
This project has been made to trace the upgrades on my Squier Telecaster Classic Vibe 50

## Original equipment
On the [Fender website](https://shop.fender.com/en-US/squier-electric-guitars/telecaster/classic-vibe-telecaster-50s/0303025507.html) you can find the original complete specs.

## Body troubles
The neck was not correctly aligned with the bridge and the 1st sting (e) was stucking on the last frets.
To fix this I dismantled the neck, added a little foil of wood on the upper side (6th string side) and forced the neck in the correct position locking the screws.

## Machine Head
I prefer to have locking machine, so I choosed to install a set of [Gotoh SD91 MG-T](https://g-gotoh.com/international/product/sd91-mg-t.html), they fitted really fine the original holes.

## Electronics
### 4-way Switch
I choosed to install a [4-way Switch](http://www.allpartsitalia.com/EP-4374-000-4-Way-Oak-Grigsby-Switch_p_9086.html) to have the following position available:

- Neck
- Bridge
- Both in parallel
- Both in series (like an humbucker)

The new position is giving a really interesting satured sound.

### Pickup
I installed a [Fender Custom Shop '51 NOcaster pickup set](https://shop.fender.com/en-US/parts/telecaster-parts/fender-custom-shop-%E2%80%9951-nocaster-tele-pickups/0992109000.html)